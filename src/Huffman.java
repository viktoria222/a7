//http://akak-ich.ru/progr-java_huffman.php

import java.sql.SQLOutput;
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data..
 */
public class Huffman {

    HashMap<String, Integer> frequencyMap = new HashMap();
    HashMap<String, Boolean> freeMap = new HashMap();
    HashMap<String, String> parentMap = new HashMap();
    private static int encodedDataInBits;
    String encodeStr;
    static String[] treeSymbolCodes;

    /**
     * Constructor to build the Huffman code for a given bytearray.
     *
     * @param original source data
     */
    Huffman(byte[] original) {

        for (int i = 0; i < original.length; i++) {

            Integer x = frequencyMap.get("c"+ original[i]);
            if (x == null) {
                frequencyMap.put("c" + original[i], 1);
            } else
                frequencyMap.put("c" + original[i], x + 1);

        }
        for (String key : frequencyMap.keySet()) {
            freeMap.put(key, true);
            parentMap.put(key, "");

        }
        Tree t = new Tree(frequencyMap, freeMap, parentMap);

    }

    /**
     * Length of encoded data in bits.
     *
     * @return number of bits
     */
    public int bitLength() {
       return encodedDataInBits;
    }

    /**
     * Encoding the byte array using this prefixcode.
     *
     * @param origData original data
     * @return encoded data
     */
    public byte[] encode(byte[] origData) {
        encodeStr = "";
        for (int i = 0; i < origData.length; i++) {
            String val = parentMap.get("c" + origData[i]);
            encodeStr += val;
        }

        return prettyBinary(encodeStr, 8, " "); // TODO!!!
    }

    public byte[] prettyBinary(String binary, int blockSize, String separator) {
        byte[] res = new byte[binary.length() / 8 + 1];
        String result;
        int index = 0;
        int i = 0;
        while (index < binary.length()) {
            result = binary.substring(index, Math.min(index + blockSize, binary.length()));
            int x = Integer.parseInt(result, 2);
            if (binary.length() - index < blockSize) {
                int pos = 1 << (8 - (binary.length() - index));
                x = x * pos;
            }
            res[i++] = (byte) x;
            index += blockSize;
        }
        return res;
    }

    /**
     * Decoding the byte array using this prefixcode.
     *
     * @param encodedData encoded data
     * @return decoded data (hopefully identical to original)
     */
    public byte[] decode(byte[] encodedData) {
        String str, decoded = "";
        encodeStr = "";
        String decodeStr = "";
        for (int i = 0; i < encodedData.length; i++) {
            str = "";
            int x = encodedData[i];
            if (x < 0) x += 256;
            str = String.format("%8s", Integer.toBinaryString(x)).replace(' ', '0');
            encodeStr += str;

        }
        do {
            int bytes = 0;
            boolean found = false;
            do {
                bytes++;
                str = encodeStr.substring(0, bytes);
                for (Map.Entry<String, String> entry : parentMap.entrySet()) {
                    if (entry.getValue().equals(str)) {
                        decoded = entry.getKey();
                        found = true;
                        break;
                    }
                }
            } while (!found);
            decoded = decoded.substring(1, decoded.length());
            char c = (char) (Integer.parseInt(decoded));
            decodeStr = decodeStr + c;
            encodeStr = encodeStr.substring(bytes, encodeStr.length());
            try {
                int end = Integer.parseInt(encodeStr);
                if (end == 0) break;
            } catch (Exception e) {

            }

        } while (encodeStr.length() > 0);
        return decodeStr.getBytes();
    }

    /**
     * Main method.
     */
    public static void main(String[] params) {
        //String tekst = "AAAAAAAAABBBBBBCCCDDEEFJK";
        //String tekst = "123AAABBC";
        //String tekst = "AAABBC";
        String tekst = "ABCDEFAAABBC";
        byte[] orig = tekst.getBytes();
        Huffman huf = new Huffman(orig);
        byte[] kood = huf.encode(orig);
        byte[] orig2 = huf.decode(kood);

        System.out.println("Input text: " + tekst);
        System.out.println("Properties of the prefixcode: ");
        System.out.println("Length of original data in bytes: " + tekst.length());
        int lngth = huf.bitLength();
        System.out.println("Length of encoded data in bits: " + lngth + " (" + Math.round(lngth * 0.125) + " bytes)");

        for (String treeSymbolCodeRow : treeSymbolCodes) {
            System.out.println( treeSymbolCodeRow);
        }

        // must be equal: orig, orig2
        System.out.println(Arrays.equals(orig, orig2));
        // TODO!!! Your tests here!
    }

    public static class Tree {
        HashMap<String, Integer> frequencyMap;
        HashMap<String, Boolean> freeMap;
        HashMap<String, String> parentMap;
        String firstMin, secondMin;
        int firstMinValue, secondMinValue;
        int trueCount = 0;
        int codeLength;

        public Tree(HashMap frequencyMap, HashMap<String, Boolean> freeMap, HashMap parentMap) {
            this.freeMap = freeMap;
            this.frequencyMap = frequencyMap;
            this.parentMap = parentMap;
            trueCount = frequencyMap.size();
            buildTree(frequencyMap, freeMap);
            initTree();
        }

        public void initTree() {
            treeSymbolCodes = new String[parentMap.keySet().size()];
            int treeKey = 0;
            for (String key : parentMap.keySet()) {
                codeLength += parentMap.get(key).length() * frequencyMap.get(key);
                treeSymbolCodes[treeKey] = ("Symbol "+(char) (Integer.parseInt(key.substring(1,key.length())))+" code: "+key.substring(1,key.length())+" frequency "+frequencyMap.get(key)+" and hcodeLength "+parentMap.get(key).length()+" bits hcode "+parentMap.get(key));
                treeKey++;
            }
            encodedDataInBits = codeLength ;
            System.out.println("end of tree");

        }


        public void buildTree(HashMap<String, Integer> frequencyMap, HashMap<String, Boolean> freeMap) {
            if (trueCount == 1) {
                for (String key : parentMap.keySet()) {
                    parentMap.put(key, "1" + parentMap.get(key));
                }
            }
            while (trueCount != 1) {
                min(frequencyMap, freeMap);
                frequencyMap.put(firstMin + secondMin, firstMinValue + secondMinValue);
                freeMap.put(firstMin + secondMin, true);
                freeMap.put(firstMin, false);
                freeMap.put(secondMin, false);
                trueCount--;
                for (String key : parentMap.keySet()) {
                    if (firstMin.contains(key))
                        parentMap.put(key, "1" + parentMap.get(key));
                    else if ((secondMin != null) && (secondMin.contains(key)))
                        parentMap.put(key, "0" + parentMap.get(key));
                }
            }
        }

        private void min(HashMap<String, Integer> frequencyMap, HashMap<String, Boolean> freeMap) {
            firstMin = null;
            secondMin = null;
            int min = 100000001;
            for (String key : frequencyMap.keySet()) {
                if ((frequencyMap.get(key) < min) && (freeMap.get(key) == true)) {
                    min = frequencyMap.get(key);
                    firstMin = key;
                    firstMinValue = min;
                }
            }
            min = 100000001;
            for (String key : frequencyMap.keySet()) {
                if ((frequencyMap.get(key) < min) && (freeMap.get(key)) && (!key.equals(firstMin))) {
                    min = frequencyMap.get(key);
                    secondMin = key;
                    secondMinValue = min;
                }
            }
        }

    }
}

